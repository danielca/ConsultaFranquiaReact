/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    RefreshControl, ScrollView, StatusBar,
    StyleSheet,
    Text, ToolbarAndroid,
    View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Circle from "react-native-progress/Circle";


export default class App extends Component<Props> {

    getData() {
        this.state = {
            refreshing: false
        };
        fetch('http://consumo.claro.com.br/')
          .then(response => {
            console.warn(response);

            return response;
            
          })
          .then(response => {
            console.debug(response);
            // ...
          }).catch(error => {
            console.error(error);
          });
    }

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            franquia: "11GB",
            utilizada: "2.36GB",
            diasFaltando: "15 dias",
            porCento: 35.0

        };
    }

    onRefresh = () => {
        this.state = {
            refreshing: true
        };
        this.getData();
    };


    render() {
        return (
            <View>
                <StatusBar
                    backgroundColor="red"
                    animated={true}
                />


                <ScrollView
                    style={styles.container}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={() => this.onRefresh()}
                        />
                    }>



                    <View style={styles.align}>
                        <Text style={[styles.text, styles.textLeft]}> <Icon style={styles.text} name="cellphone"/>Franquia:</Text>
                        <Text style={[styles.text, styles.textRight]}>{this.state.franquia}</Text>

                    </View>

                    <View style={styles.align}>

                        <Text style={[styles.text, styles.textLeft]}> <Icon style={styles.text} name="chart-histogram"/>Franquia
                            Utilizada:</Text>
                        <Text style={[styles.text, styles.textRight]}>{this.state.utilizada}</Text>

                    </View>

                    <View style={styles.align}>
                        <Text style={[styles.text, styles.textLeft]}> <Icon style={styles.text} name="calendar-clock"/>Fechamento
                            ciclo:</Text>
                        <Text style={[styles.text, styles.textRight]}>{this.state.diasFaltando}</Text>

                    </View>


                    <View>
                        <Text style={[styles.text, styles.textLeft]}> <Icon style={styles.text} name="chart-pie"/>Gráfico
                            de uso:</Text>

                        <View style={styles.graph}>

                            <Circle animated={true} strokeCap={"round"} color={"red"} unfilledColor={"#d3d3d3"}
                                    borderWidth={0} thickness={10} size={150} progress={this.state.porCento / 100}/>
                            <Text style={styles.perCentText}>{this.state.porCento}%</Text>
                        </View>


                    </View>

                </ScrollView>
            </View>


        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFF',
    },
    text: {
        fontSize: 22,
        color: '#000',
        fontFamily: 'Cochin'

    },
    textRight: {
        marginRight: 10,
    },
    align: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textLeft: {
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10
    },
    graph: {
        alignItems: 'center',
        justifyContent: 'center',

    },
    perCentText: {
        position: 'absolute',
        fontSize: 30,
        color: '#000',

    }


});
